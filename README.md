# Robby The Third

## What is Robby?
Robby is a robot, he has an Arduino Nano as his brain, which can be programmed using the Arduino IDE. He has two DC motors which are controlled by a DRV8833 motor driver.
He is powered by a 2 cell, 18650 7.4v battery, with built in charger.
He has sensors on the front and back, 2 x HC-SR04 Sonar units and two IR obstacle sensors.

## Coding Prerequisites
1. To code robby you will first need to install the Arduino IDE from the [Arduino website](https://www.arduino.cc/en/software).
2. A driver is needed for the Arduino IDE to talk to the Arduino Nano microcontroller. CH340 Driver for [Windows](https://realrobots.net/files/CH341SER.EXE), [Mac](https://realrobots.net/files/CH341SER_MAC.ZIP) or [Linux](https://realrobots.net/files/CH341SER_LINUX.ZIP).
3. From within the Arduino IDE, a library for the radio must be downloaded. 
    
    From the top of the IDE select Sketch->Include Library->Manage Libraries.. 
    
    Search for the "RF24" library by TMRh20 and press the "Download" button.

Once this is done, you can download this repository and open robby_v3.ino in the Arduino IDE.
Make sure to pick the Arduino Nano in Tools->Boards.
Plug in Robby (Make sure Robby is switched on with his power switch before plugging in USB)
Select the correct port in Tools->Ports.
Press UPLOAD.

## Changing the Code
By default, the code below does nothing except get things ready.


```c++
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Robby v3");
  Serial.println("Waking up...");
  InitPins();

  EnableMotors();
}

void loop() {
  //SetMotor(0,0);


  delay(10);
}
```

To make Robby move, you need to use the SetMotor(left, right) command. Setting left and right to 150 will make him move forward.
```c++ 
SetMotor(150, 150) 
```
If you want to test out his motion, we can make him move forward, then backwards.
```c++ 
void loop() {
    SetMotor(150, 150);  // Move forward
    delay(2000);         // Robby will keep moving forward for 2 seconds
    SetMotor(0, 0);      // Stop
    delay(2000);         // Stay stopped for 2 seconds
    SetMotor(-150, -150);// Reverse both motors 
    delay(2000);         // continue reversing for 2 seconds.   
    SetMotor(0, 0);      // Stop
    delay(2000);         // Stay stopped for 2 seconds
}

```

If you want Robby to turn, just set one motor forward and the other back.
```c++
SetMotor(150, -150);
```

In the future we will learn how to use Robbies sensors, and how to use that information to control his movement.