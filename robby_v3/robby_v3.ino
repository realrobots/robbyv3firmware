

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Robby v3");
  Serial.println("Waking up...");
  InitPins();

  EnableMotors();
}

void loop()
{
  UpdateSonarDistance();  // Trigger both sonar sensors and update variables

  // If obstacle in front of both sensors
  if (GetSonarLeft() < 30 && GetSonarRight() < 30)
  {
    SetMotor(-155, 0);
    delay(500);
    
  }
  else if (GetSonarLeft() < 30) // If barrier on the left
  {
    SetMotor(155, -155);
    
  }
  else if (GetSonarRight() < 30) // If barrier on the right
  {
    SetMotor(-155, 155);

  }

  delay(10);
}
