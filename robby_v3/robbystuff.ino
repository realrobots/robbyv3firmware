#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define DISABLE_MOTORS false

RF24 radio(9, 8);  // CE, CSN
const byte address[6] = "00001";
long lastReceive = 1000;

struct data {
  int hover;
  int thrust;
  int dir;
};
data send_data;

#define PIN_IR_LEFT A1
#define PIN_IR_RIGHT A0
#define PIN_SONAR_LEFT A3
#define PIN_SONAR_RIGHT A2
#define PIN_SONAR_TRIG_LEFT 3
#define PIN_SONAR_TRIG_RIGHT 4

//#define PIN_MOT_ULT 10
#define PIN_MOT_EEP 10
#define PIN_MOT_IN1 2
#define PIN_MOT_IN2 5
#define PIN_MOT_IN3 6
#define PIN_MOT_IN4 7




long duration; // variable for the duration of sound wave travel

int leftDistance = 0;
int rightDistance = 0;

bool leftObstacle = false;
bool rightObstacle = false;

void InitPins(){
  Serial.println("Setting pinModes");
  pinMode(PIN_IR_LEFT, INPUT);
  pinMode(PIN_IR_RIGHT, INPUT);
  pinMode(PIN_SONAR_LEFT, INPUT);
  pinMode(PIN_SONAR_RIGHT, INPUT);

  pinMode(PIN_SONAR_TRIG_LEFT, OUTPUT);
  pinMode(PIN_SONAR_TRIG_RIGHT, OUTPUT);

  pinMode(PIN_MOT_EEP, OUTPUT);
  pinMode(PIN_MOT_IN1, OUTPUT);
  pinMode(PIN_MOT_IN2, OUTPUT);
  pinMode(PIN_MOT_IN3, OUTPUT);
  pinMode(PIN_MOT_IN4, OUTPUT);

  digitalWrite(PIN_MOT_IN1, LOW);
  digitalWrite(PIN_MOT_IN2, LOW);

  digitalWrite(PIN_MOT_IN3, LOW);
  digitalWrite(PIN_MOT_IN4, LOW);
  DisableMotors();
}

void ReportSonarDistance() {
  Serial.print(leftDistance);
  Serial.print("cm\t");
  Serial.print(rightDistance);
  Serial.println("cm");
}

void ReportIR() {
  Serial.print(leftObstacle);
  Serial.print("\t");
  Serial.print(rightObstacle);
  Serial.println();
}

void EnableMotors() {
  digitalWrite(PIN_MOT_EEP, HIGH);
}

void DisableMotors() {
  digitalWrite(PIN_MOT_EEP, LOW);
}

void InitRadio(){
  

  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }
  radio.openReadingPipe(0, address);
  //Set module as receiver
  radio.startListening();
  Serial.println("Radio Initialized");
}

void UpdateRadio(){
  if (radio.available())
  {
    radio.read(&send_data, sizeof(data));
    EnableMotors();
//    if (send_data.thrust < 120){
//      send_data.thrust = 120;
//    }
    
    Serial.print(send_data.hover);
    Serial.print("\t");
    Serial.print(send_data.thrust);
    Serial.print("\t");
    Serial.println(send_data.dir);
    lastReceive = millis();

    ThumbstickToTankControl();
  }

  if (millis() - lastReceive > 1000) {
    // When no radio signal for > 1 second
    send_data.dir = 127;
    send_data.thrust = 120;
    DisableMotors();
  }
}


void UpdateIR() {
  leftObstacle = !digitalRead(PIN_IR_LEFT);
  rightObstacle = !digitalRead(PIN_IR_RIGHT);
}

int GetSonarLeft(){
  return leftDistance;
}

int GetSonarRight(){
  return rightDistance;
}

void UpdateSonarDistance() {
  // Clears the trigPin condition
  digitalWrite(PIN_SONAR_TRIG_LEFT, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(PIN_SONAR_TRIG_LEFT, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_SONAR_TRIG_LEFT, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(PIN_SONAR_LEFT, HIGH);
  // Calculating the distance
  leftDistance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)


  digitalWrite(PIN_SONAR_TRIG_RIGHT, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(PIN_SONAR_TRIG_RIGHT, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_SONAR_TRIG_RIGHT, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(PIN_SONAR_RIGHT, HIGH);
  // Calculating the distance
  rightDistance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  // Displays the distance on the Serial Monitor
}



void ManualControl() {
  if (send_data.dir < 100) {
    //TURN LEFT
    
    digitalWrite(PIN_MOT_IN1, HIGH);
    digitalWrite(PIN_MOT_IN2, LOW);
  } else if (send_data.dir > 155) {
    digitalWrite(PIN_MOT_IN3, HIGH);
    digitalWrite(PIN_MOT_IN4, LOW);
    digitalWrite(PIN_MOT_IN1, LOW);
    digitalWrite(PIN_MOT_IN2, HIGH);
  } else if (send_data.thrust > 155) {
    digitalWrite(PIN_MOT_IN3, HIGH);
    digitalWrite(PIN_MOT_IN4, LOW);
    digitalWrite(PIN_MOT_IN1, HIGH);
    digitalWrite(PIN_MOT_IN2, LOW);
  } else if (send_data.thrust < 100) {
    digitalWrite(PIN_MOT_IN3, LOW);
    digitalWrite(PIN_MOT_IN4, HIGH);
    digitalWrite(PIN_MOT_IN1, LOW);
    digitalWrite(PIN_MOT_IN2, HIGH);
  } else {
    digitalWrite(PIN_MOT_IN3, LOW);
    digitalWrite(PIN_MOT_IN4, LOW);
    digitalWrite(PIN_MOT_IN1, LOW);
    digitalWrite(PIN_MOT_IN2, LOW);
  }
}

// Sets left and right motor speeds, 255=full forward, 0=stop, -255=full reverse
void SetMotor(int l, int r){
  if (DISABLE_MOTORS){
    l = 0;
    r = 0;
  }

  if (l > 0) {    
    analogWrite(PIN_MOT_IN3, l);
    digitalWrite(PIN_MOT_IN4, 0);
  } else if (l < 0) {
    l = -l;
    analogWrite(PIN_MOT_IN3, 255-l);
    digitalWrite(PIN_MOT_IN4, 1);
  } else {
    digitalWrite(PIN_MOT_IN3, 0);
    digitalWrite(PIN_MOT_IN4, 0);
  }
  if (r > 0) {
    analogWrite(PIN_MOT_IN2, 255-r);
    digitalWrite(PIN_MOT_IN1, 1);
  } else if (r < 0) {
    r=-r;
    analogWrite(PIN_MOT_IN2, r);
    digitalWrite(PIN_MOT_IN1, 0);
  } else {
    digitalWrite(PIN_MOT_IN2, 0);
    digitalWrite(PIN_MOT_IN1, 0);
  }


}


void ThumbstickToTankControl() {
  int x = -map(send_data.dir, 0, 255, 100, -100);
  if (x > -25 && x < 25)
    x = 0;
  int y = map(send_data.thrust, 0, 255, -100, 100);
  if (y > -25 && y < 25)
    y = 0;

  int l = (x + y);
  int r = (y - x);

  if (l > 100)
  {
    int diff = l - 100;
    l = l - diff;
    r += diff / 2;
  }

  if (r > 100)
  {
    int diff = r - 100;
    r = r - diff;
    l += diff / 2;
  }
  int temp = 0;

  if (r < -100)
  {
    int diff = -r - 100;
    r = r + diff;
    l -= diff / 2;
  }

  if (l < -100)
  {
    int diff = -l - 100;
    l = l + diff;
    r -= diff / 2;
  }

  r = map(r, -100, 100, -255, 255);
  l = map(l, -100, 100, -255, 255);

  Serial.print(r);
  Serial.print("\t");
  Serial.print(l);
  Serial.println();

  SetMotor(l, r);
}
